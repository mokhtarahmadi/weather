package com.ahmadi.mokhtar.weatherapp.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Clouds(
     var all: Int
):Parcelable
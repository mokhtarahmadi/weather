package com.ahmadi.mokhtar.weatherapp.di.component

import androidx.annotation.Keep
import com.ahmadi.mokhtar.weatherapp.di.modules.ApiModule
import com.ahmadi.mokhtar.weatherapp.di.modules.AppModule
import com.ahmadi.mokhtar.weatherapp.view.activities.MainActivity
import com.ahmadi.mokhtar.weatherapp.viewModel.WeatherViewModel
import dagger.Component
import javax.inject.Singleton

@Keep
@Singleton
@Component(modules = [AppModule::class , ApiModule::class])
interface DIComponent {

    interface Injectable{
        fun inject(diComponent: DIComponent)
    }

    fun inject(mainActivity: MainActivity)
    fun inject(weatherViewModel: WeatherViewModel)
}
package com.ahmadi.mokhtar.weatherapp.di.modules

import android.content.Context
import androidx.room.Room
import com.ahmadi.mokhtar.weatherapp.App
import com.ahmadi.mokhtar.weatherapp.data.database.AppDatabase
import com.ahmadi.mokhtar.weatherapp.data.database.CityDao
import com.ahmadi.mokhtar.weatherapp.data.network.Api
import com.ahmadi.mokhtar.weatherapp.data.repository.WeatherRepo
import com.ahmadi.mokhtar.weatherapp.data.repository.WeatherRepository
import com.ahmadi.mokhtar.weatherapp.utils.AppConstants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    fun providesApp(): App = app

    @Provides
    @Singleton
    internal fun provideContext(): Context =app.applicationContext


    @Singleton
    @Provides
    internal fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.DB_NAME).build()
    }


    @Singleton
    @Provides
    internal fun providesCityAppDataBaseDao(database: AppDatabase): CityDao {
        return database.cityDao
    }


    @Singleton
    @Provides
    internal fun provideWeatherRepository(context: Context, dataBase: AppDatabase, api: Api): WeatherRepository {
        return WeatherRepository(context, dataBase, api)
    }

    @Singleton
    @Provides
    internal fun provideWeatherRepo(weatherRepository: WeatherRepository): WeatherRepo {
        return weatherRepository
    }


}
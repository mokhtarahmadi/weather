package com.ahmadi.mokhtar.weatherapp

import android.app.Application
import com.ahmadi.mokhtar.weatherapp.di.component.DIComponent
import com.ahmadi.mokhtar.weatherapp.di.component.DaggerDIComponent
import com.ahmadi.mokhtar.weatherapp.di.modules.ApiModule
import com.ahmadi.mokhtar.weatherapp.di.modules.AppModule

class App : Application() {

    lateinit var di : DIComponent

    override fun onCreate() {
        super.onCreate()

        di = DaggerDIComponent
            .builder()
            .apiModule(ApiModule())
            .appModule(AppModule(this))
            .build()

    }
}
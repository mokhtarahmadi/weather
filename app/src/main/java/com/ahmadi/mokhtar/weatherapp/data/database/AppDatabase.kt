package com.ahmadi.mokhtar.weatherapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ahmadi.mokhtar.weatherapp.data.models.City

@Database(entities = [City::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val cityDao: CityDao
}
package com.ahmadi.mokhtar.weatherapp.data.repository

import com.ahmadi.mokhtar.weatherapp.data.models.WeatherResult
import io.reactivex.Observable

interface WeatherRepo {

    fun getWeather(name: String): Observable<WeatherResult>
}
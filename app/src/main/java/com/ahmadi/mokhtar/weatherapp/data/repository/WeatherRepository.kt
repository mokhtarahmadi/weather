package com.ahmadi.mokhtar.weatherapp.data.repository

import android.content.Context
import com.ahmadi.mokhtar.weatherapp.data.database.AppDatabase
import com.ahmadi.mokhtar.weatherapp.data.models.WeatherResult
import com.ahmadi.mokhtar.weatherapp.data.network.Api
import com.ahmadi.mokhtar.weatherapp.utils.AppConstants
import io.reactivex.Observable
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val context: Context, private val dataBase: AppDatabase, private val api: Api) : WeatherRepo {

    override fun getWeather(name: String): Observable<WeatherResult> {
        return api.getWeather(name, AppConstants.API_KEY)
    }
}